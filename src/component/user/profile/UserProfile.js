import { Button, Form, Input, Upload } from "antd";

import React, { useState } from "react";
// import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { message } from "antd";
import { AntdInput, AntdUploder, SaveButtton } from "../../common";
// const getBase64 = (img, callback) => {
//   const reader = new FileReader();
//   reader.addEventListener("load", () => callback(reader.result));
//   reader.readAsDataURL(img);
// };

// const beforeUpload = (file) => {
//   const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
//   if (!isJpgOrPng) {
//     message.error("You can only upload JPG/PNG file!");
//   }
//   const isLt2M = file.size / 1024 / 1024 < 2;
//   if (!isLt2M) {
//     message.error("Image must smaller than 2MB!");
//   }
//   return isJpgOrPng && isLt2M;
// };

const UserProfile = () => {
  // const [loading, setLoading] = useState(false);
  // const [imageUrl, setImageUrl] = useState();
  // const handleChange = (info) => {
  //   if (info.file.status === "uploading") {
  //     setLoading(true);
  //     return;
  //   }
  //   if (info.file.status === "done") {
  //     // Get this url from response in real world.
  //     getBase64(info.file.originFileObj, (url) => {
  //       setLoading(false);
  //       setImageUrl(url);
  //     });
  //   }
  // };
  // const uploadButton = (
  //   <button
  //     style={{
  //       border: 0,
  //       background: "none",
  //     }}
  //     type="button"
  //   >
  //     {loading ? <LoadingOutlined /> : <PlusOutlined />}
  //     <div
  //       style={{
  //         marginTop: 8,
  //       }}
  //     >
  //       Upload
  //     </div>
  //   </button>
  // );

  const handelOnfinish =(value)=>{
    console.log('value',value)
  }

  return (
    <div>
      <Form layout="vertical" onFinish={handelOnfinish}>
        <div className="flex justify-between">
          <div className="grid gap-x-2 md:grid-cols-12 grid-flow-row">
            <div className="md:col-span-4">
             <AntdInput name='name' label="First Name" required/>
            </div>

            <div className="md:col-span-4">
              
             <AntdInput name='last_name' label="Last Name" required/>
                
              
            </div>

            <div className="md:col-span-4">
             <AntdInput name='email' label="Email" required/>
              
            </div>

            <div className="md:col-span-4">
             <AntdInput name='contact' label="contact" required/>
              
            </div>

            <div className="md:col-span-4">
             <AntdInput name='address' label="Address" required/>
              
            </div>

            <div className="md:col-span-4 gap-2 flex">
              <div>
                <SaveButtton type="submit" loading={true}/>
              </div>

              <Button className="bg-green-500">Edit</Button>
            </div>
          </div>

          <div className="w-[10%] justify-end flex">
            <AntdUploder name ="photo" />
            
              
              {/* <Upload
          name="avatar"
          listType="picture-circle"
          className="avatar-uploader"
          showUploadList={false}
          action="https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188"
          beforeUpload={beforeUpload}
          onChange={handleChange}
        >
          {imageUrl ? (
            <img
              src={imageUrl}
              alt="avatar"
              style={{
                width: '100%',
              }}
            />
          ) : (
            uploadButton
          )}
            

          </Upload> */}
            
          </div>
        </div>
      </Form>
    </div>
  );
};

export default UserProfile;