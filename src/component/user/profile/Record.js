import React from "react";
import { Table } from "antd";

const Record = () => {
  const [tableParams,setTableParams]=React.useState({
    current:1,
    pazeSize:5,
    total:150,
  })
  const handlePaginatonChange=(paginationData)=>{
    setTableParams({
      current:paginationData?.current,
      pazeSize:paginationData?.pazeSize,
      total:paginationData?.total,
    })

  }
  const columns = [
    {
      title: "SN",
    
    render: (text,record,index) =>{
        return(
          <div>
            {(tableParams?.current-1)*tableParams.pazeSize+(index+1)}
          </div>
        )
      }
    }
    ,
    
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Tags",
      key: "tags",
      dataIndex: "tags",
    },
  ];
  const data = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      tags: ["nice", "developer"],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      tags: ["loser"],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sydney No. 1 Lake Park",
      tags: ["cool", "teacher"],
    },
    {
      key: "4",
      name: "Eden Hazard",
      age: 32,
      address: "belgium",
      tags: ["cool", "footballer"],
    },
    {
      key: "5",
      name: "Lampard",
      age: 32,
      address: "England",
      tags: ["cool", "Manager"],
    },
    {
      key: "6",
      name: "Conte",
      age: 32,
      address: "germany",
      tags: ["cool", "manager"],
    },
  ];
  return (
    <div>
      <Table columns={columns} dataSource={data}  pagination={{
        // rowkey:{key},
        current:tableParams.current,
        pageSize:tableParams.pazeSize,
        total:tableParams.total,
        showQuickJumper:true,
        showLessItems:true,
      }} 
      onChange={handlePaginatonChange}/>
    </div>
  );
};

export default Record;
