import { EyeOutlined } from "@ant-design/icons";
import { Button, Card, Form, Rate } from "antd";
import React from "react";
import { useAppContext } from "../../ContextApi";
import { AntdInput, SaveButtton } from "../common";
import Order from "./Order";

const UserDetails = () => {
  const { appState } = useAppContext();
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [isvalue, setIsValue] = React.use({
    price:0,
    sum:0

  })
  console.log("userdetail", appState);
  // const myValue = localStorage.getItem("userdetail");
  // const data = JSON.parse(myValue);
  // console.log("userdetail", JSON.parse(myValue));
  const handleByNow=(value)=>{
    setIsModalOpen(true)
    setIsValue({qty:value?.Qty,
      price:appState?.detail?.price*value.Qty,
      name:appState?.detail?.name
    })
    console.log(value)

  }
  return (
    <div className="flex justify-center gap-2.5">
      <div>
        <Card
          hoverable
          style={{
            width: 440,
          }}
          cover={<img alt="example" src={appState.detail.image} />}
        >
          <div>
            <Button type="primary">Add to Cart</Button>
          </div>
        </Card>
      </div>
      <div
        className="border-2 border-gray w-1/4 p-3"
        // style={{
        //   boxSizing: "border-box",
        //   borderRadius: "1px",
        //   borderWidth: "1px",
        //   borderBlockColor: "black",
        // }}
      >
        <div className="text-xl font-bold">{appState.detail.name}</div>
        <div>Price:{appState.detail.price}</div>
        <div>Brand:{appState.detail.brand}</div>

        <div>
          <Rate value={appState.detail.rating} />
        </div>

        <div>
          <EyeOutlined />
          View:{appState.detail.view}
        </div>
        <div>Stock Available:{appState.detail.stockItem}</div>
        <div>Description:{appState.detail.description}</div>
        <div className=" w-full">
        <Form onFinish={handleByNow} >
        <AntdInput initialValue={1} name="Qyt"/>
        <SaveButtton type="submit" name="buy now"/>
        <div>
        <SaveButtton type="submit" name="add to cart"/>
        </div>
        
        </Form>
        
        </div>
        {isModalOpen && (
                <Order
                  isModalOpen={isModalOpen}
                  setIsModalOpen={(e) => setIsModalOpen(e)}
                  sumNetTotal={isvalue?.price}
                   sumQt={isvalue?.qty}
                   myOrder={isvalue}
                />
              )}

      </div>

    </div>
  );
};

export default UserDetails;
